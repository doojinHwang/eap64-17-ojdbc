FROM registry.access.redhat.com/jboss-eap-6/eap64-openshift:1.7

MAINTAINER Yun In Su <ora01000@time-gate.com>

RUN mkdir -p /opt/eap/modules/system/layers/openshift/com/oracle/main
COPY module.xml /opt/eap/modules/system/layers/openshift/com/oracle/main
COPY ojdbc.jar /opt/eap/modules/system/layers/openshift/com/oracle/main
COPY ./configuration/standalone-openshift.xml /opt/eap/standalone/configuration
#COPY openshift-launch.sh /opt/eap/bin
COPY tx-datasource.sh /opt/eap/bin/launch
COPY datasource-common.sh /opt/eap/bin/launch

